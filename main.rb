
require "rubygems"
require "sequel"
require "date"
require "yaml"

# Work in progress - using this to prototype blending
# data from 2 separate databases together with some biz logic for finding
# most likely registers to map

# will improve this to use command line
### FOR NOW CHANGE customer to be the one of choice and make sure
### it has an entry in the settings.yaml
customer = "usawireless"

conf = YAML.load_file("conf/settings.yaml")

NEWDEPLOYDB = Sequel.connect(conf["new_deploy"]["db"])
BIDB = Sequel.connect(conf[customer]["db"])
domain = conf[customer]["domain"]


register_transaction_type_counts_query =
<<~HEREDOC
select site_id, workstation_id, transaction_type, business_day_date, count(*) as transaction_count from
  transaction, transaction_type
  where trans_type_id = transaction_type.transaction_type_id
group by site_id, workstation_id, transaction_type
order by workstation_id, transaction_type
HEREDOC

register_activity_query =
<<~HEREDOC
  select s.site_id, s.retail_store_code, s.retail_store_name, w.workstation_id, w.workstation_code as register_number, smr.transaction_count, smr.business_day_date as most_recent_transaction from site s, workstation w,
  (select site_id, workstation_id, business_day_date, count(*) as transaction_count from
    (select site_id, workstation_id, business_day_date from transaction
      order by business_day_date desc) i
  group by site_id, workstation_id)  smr
  where s.site_id = smr.site_id
  and s.active = 1
  and w.workstation_id = smr.workstation_id
  order by retail_store_name, transaction_count desc
HEREDOC

registers_mapped_to_cameras_query =
<<~HEREDOC
  select domain.id, domain.name, app_physical_data.apd_app_name, acd_cam_id, app_cam_def.acd_name as camera_name,
  registers.name, registers.register_number, CONCAT(apd_app_name,":",register_number) as store_register_concat
  from app_physical_data, camera_register_relation, app_cam_def, domain, registers
  where domain.id = app_physical_data.domain_id
  and domain.name="#{domain}"
  and app_physical_data.apd_phy_id = app_cam_def.acd_phy_id
  and app_cam_def.acd_row_id = camera_register_relation.camera_row_id
  and registers.id = camera_register_relation.register_id
  order by app_physical_data.apd_app_name, registers.register_number
HEREDOC

def print_description
  puts "UNMAPPED REGISTERS that have 10 or more combined Sales and Exchanges and report a transaction in the last 30 days"
end

def print_header
  puts "Site Id\tWorkstation Id\tLocation\tRegister Number\tNumber of Transactions\tMost Recent Transaction\tBill Pays\tReturns\tSales\tExchanges"
end

def get_txn_count(row, txn_counts, name)
  txn_counts[row[:workstation_id]][name] || 0
end

def print_row(row, txn_counts)

  print "#{row[:site_id]}\t#{row[:workstation_id]}\t#{row[:retail_store_name]}\t#{row[:register_number]}\t#{row[:transaction_count]}\t#{row[:most_recent_transaction]}\t"
  print "#{get_txn_count(row, txn_counts,'Bill Payment')}\t#{get_txn_count(row, txn_counts,'Return')}\t"
  puts  "#{get_txn_count(row, txn_counts,'Sale')}\t#{get_txn_count(row, txn_counts,'Exchange')}"
end


ds = NEWDEPLOYDB[registers_mapped_to_cameras_query]

rows = ds.call(:select)

if rows.length < 1
  puts "WARNING NO CAMERA DATA, PROBLEM!"
  #exit
end


mapped = Hash[rows.map {|e| [e[:store_register_concat].downcase.gsub(/\s+/, ''), e]}]

ds = BIDB[register_activity_query]
rows = ds.call(:select)


register_activity = Hash[rows.map {|e| [(e[:retail_store_name] + ":"+e[:register_number]).downcase.gsub(/\s+/, ''), e]}]

rc  = BIDB[register_transaction_type_counts_query]
rows = rc.call(:select)


register_transaction_counts = {}
rows.each do |r|
  register_transaction_counts[r[:workstation_id]] = {} if register_transaction_counts[r[:workstation_id]] == nil
  register_transaction_counts[r[:workstation_id]][r[:transaction_type]] = r[:transaction_count]
end

# Go through each active register, look it up in the mapped list
today = Date.today
count = 0

print_description
print_header

register_activity.each do |k,v|
  sales_count = get_txn_count(v, register_transaction_counts, 'Sale')
  exchange_count = get_txn_count(v, register_transaction_counts, 'Exchange')
  yes_include = (sales_count + exchange_count > 10)
  if !mapped[k] && yes_include && today - v[:most_recent_transaction] < 30
    # we have a miss, print it out
    count= count +1
    print_row v, register_transaction_counts
  end
end

puts ">>>>>>>>>>>ALL UNMAPPED"
print_header
register_activity.each do |k,v|
  if !mapped[k] && today - v[:most_recent_transaction] < 30
    # we have a miss, print it out
    count= count +1
    print_row v, register_transaction_counts
  end
end
