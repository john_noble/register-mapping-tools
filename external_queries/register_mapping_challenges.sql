-- Going to pull some transaction data to see if we can find
-- non overlapping transactions to help cue video watching
-- for single transactions at register so we can see what register
-- is in use
select * from site where retail_store_name regexp "1764"

-- above yields site ID 8116

-- get transaction data
select w.workstation_code as register_number , t.*, s.retail_store_name from transaction t, workstation w, site s where t.site_id = 8116
and w.workstation_id = t.workstation_id
and s.site_id = t.site_id
and business_day_date >= '2016-05-31' order by begin_date_time

--- Here is the query to get us number of transactions per register - REGISTER ACTIVITY
--- maybe need to adjust these with the "Active" bool flag?
select s.site_id, s.retail_store_code, s.retail_store_name, w.workstation_id, w.workstation_code as register_number, smr.transaction_count, smr.business_day_date as most_recent_transaction from site s, workstation w,
(select site_id, workstation_id, business_day_date, count(*) as transaction_count from
	(select site_id, workstation_id, business_day_date from transaction order by business_day_date desc) i
group by site_id, workstation_id)  smr
where s.site_id = smr.site_id
and w.workstation_id = smr.workstation_id
order by retail_store_name, transaction_count desc


---- First Transaction of the Day
-- First Transactions of the Day by Store

select i.* from
(select s.site_id, s.retail_store_name, s.retail_store_code, t.receipt_number, t.business_day_date, t.begin_date_time, t.end_date_time, CAST(w.workstation_code as UNSIGNED) as register_number
from site s, transaction t, workstation w
where s.active = 1
and t.site_id = s.site_id
and t.workstation_id = w.workstation_id
and t.business_day_date >= '2016-11-15'
order by site_id, begin_date_time) i
group by site_id, business_day_date
order by retail_store_name, register_number, business_day_date


---- Last transaction of the Day
select i.* from
(select s.site_id, s.retail_store_name, s.retail_store_code, t.receipt_number, t.business_day_date, t.begin_date_time, t.end_date_time, CAST(w.workstation_code as UNSIGNED) as register_number
from site s, transaction t, workstation w
where s.active = 1
and t.site_id = s.site_id
and t.workstation_id = w.workstation_id
and t.business_day_date >= '2016-11-15'
order by site_id, begin_date_time desc) i
group by site_id, business_day_date
order by retail_store_name, register_number, business_day_date


---- Problem with CashIns having different register numbers ugh - Translation
--- Need to run this on the raw API data and have the current register mapping
--- file imported into the DB
select f.store_name, f.store_id, f.workstation_name, f.terminal_id, f.register_number as cash_in_register_number, wm.register_number as map_to_transaction_register from workstation_register_mapping wm,
(select distinct s.store_name, r.* from stores s,
(select ws.*, m.* from workstation_register_mapping m,
(select distinct terminal_id, workstation_name from sale_invoice_products where customer="Ntouch" ) ws
where m.customer="Ntouch"
and CAST(m.workstation as UNSIGNED) > 0
and CAST(m.workstation as UNSIGNED) = ws.terminal_id) r
where r.store_id = s.store_id
) f
where
f.store_id = wm.store_id
and f.workstation_name = wm.workstation
order by store_name, f.register_number
