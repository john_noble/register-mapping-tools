
-- The results from these 2 queries are compared for a difference.
-- Registers seen in transactions thate are not found in the camera to
-- register mapping MEANS A POTENTIAL GAP IN MAPPING.

-- We are doing this in a Google Sheet now - Example:
-- https://docs.google.com/a/envysion.com/spreadsheets/d/1ltMCSypb0xHYdXoaOmDHv3x1mgQ5VEatrDy4YlELO_Q/edit?usp=sharing


-- webhost.prod1.nakika.tv
-- DB: nakika_newdeploy
--
-- This query gives us per site camera to register mapping for the given customer domain:
-- The results to into the Cameras Currently Mapped tab in the above Google
-- sheet example


select domain.id, domain.name, app_physical_data.apd_app_name, acd_cam_id, app_cam_def.acd_name as camera_name,
registers.name, registers.register_number, CONCAT(apd_app_name,":",register_number) as store_register_concat
from app_physical_data, camera_register_relation, app_cam_def, domain, registers
where domain.id = app_physical_data.domain_id
and domain.name="awireless.com"
and app_physical_data.apd_phy_id = app_cam_def.acd_phy_id
and app_cam_def.acd_row_id = camera_register_relation.camera_row_id
and registers.id = camera_register_relation.register_id
order by app_physical_data.apd_app_name, registers.register_number


-- Next Need to do this on BI for the same domain as above
-- This query gives us the registers seen in transaction data grouped by site:

select w.workstation_id, w.site_id, cast(workstation_code as UNSIGNED) as register_number, site.retail_store_name,
CONCAT(retail_store_name,":",cast(workstation_code as UNSIGNED)) as store_register_concat
from workstation w, site, (
select t.site_id, workstation_id from transaction t, dim_site s where t.site_id = s.site_id group by t.site_id, workstation_id) a
where a.workstation_id = w.workstation_id and site.site_id = a.site_id

--- This one adds the most recent transaction date
select w.workstation_id, w.site_id, cast(workstation_code as UNSIGNED) as register_number, site.retail_store_name,
CONCAT(retail_store_name,":",cast(workstation_code as UNSIGNED)) as store_register_concat,
business_day_date as most_recent_transaction from workstation w, site, (
select i.* from
(select t.site_id, workstation_id,  t.business_day_date from transaction t, dim_site s where t.site_id = s.site_id order by business_day_date desc) i group by i.site_id, workstation_id) a
where a.workstation_id = w.workstation_id and site.site_id = a.site_id

-- Modified to use "active site" and to exclude bill pay and return transactions in the counts
select s.site_id, s.retail_store_code, s.retail_store_name, w.workstation_id, w.workstation_code as register_number, smr.transaction_count, smr.business_day_date as most_recent_transaction from site s, workstation w,
  (select site_id, workstation_id, business_day_date, count(*) as transaction_count from
  	(select site_id, workstation_id, business_day_date from transaction
      where trans_type_id not in (
        select transaction_type_id from transaction_type
        where transaction_type regexp "^Bill Pay|^Return"
      )
      order by business_day_date desc) i
  group by site_id, workstation_id)  smr
  where s.site_id = smr.site_id
  and s.active = 1
  and w.workstation_id = smr.workstation_id
  order by retail_store_name, transaction_count desc
-- The results of this query go into the Active Regsiters Seen in transactions
-- tab

-- The results then come in the Mapped vs. Transaction Report
